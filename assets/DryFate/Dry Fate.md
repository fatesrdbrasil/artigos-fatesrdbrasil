---
tags: Fate, Hack, Minimalista
---

# Dry Fate

## Um Hack para Fate Básico 

### Por Fábio Emilio Costa

+ ___Fate Core___ (CC-BY) 2012 Evil Hat Production; (CC-BY) 2015 Solar Entretenimento; (CC-BY) 2018 Coletivo Solar

## O Objetivo do Dry Fate

_Dry Fate_ (Fate Seco) é um hack que criei tentando desenvolver uma versão minimalista do Fate que, ainda que minimalista, contenha o que eu considero os Pilares do Fate. Ele utiliza regras obtidas em diversos módulos, como _Ferramentas de Sistema, Uprising – The Dystopian Universe RPG, Boa Vizinhança (Good Neighbors)_ e assim por diante.

> __*O que* Dry Fate NÃO É?__
> 
> _Dry Fate_ não visa ser um substituto completa ao Fate Básico ou ao Fate Acelerado, e sim um jogo minimalista e que sirva de introdução às bases do Fate. É altamente recomentado que você leia o _Fate Básico_ e o _Fate Acelerado_ (disponíveis na [MeepleBR (Físico)](https://www.lojameeplebrjogos.com.br/rpg) ou na [Dungeonist (Digital)](https://www.dungeonist.com/marketplace/?post_type=product&s=fate)) após alguns jogos para sanar possíveis dúvidas e conhecer ainda melhor o Fate.
>
> __*O que* Dry Fate não abarca?__
>
> Algumas regras e dicas para criação de aventuras. De outra forma, Dry Fate é um jogo completo e compatível com qualquer produto _Movido Pelo Fate_

## Criar Personagem

### Aspectos

Aspectos são frases que definem características importantes e únicas sobre o seu personagem ou qualquer outro elemento do cenário, representando Verdades no jogo enquanto os mesmos estão presentes. Aspectos costumam ser _ambíguos_ (tendo pontos positivos e negativos), _claros_ e _descritivos_.

Em _Dry Fate_, todos os Aspectos de personagem devem descrever também algum tipo de ___Ocupação___. Essa ocupação pode ser declarada de maneira direta, como em ___Policial Indomável___ ou ___Fotógrafo Investigativo___, ou de maneira indireta ou mesmo ampla, como no fato de ele ser um ___Palhação que não suporta ver uma criança chorar___.

Defina Cinco Aspectos de Personagem. Desses, três representam o ___Conceito___ (o que o personagem fundamentalmente é), ___Dificuldade___ (o maior problema do personagem, que funciona como um freio para suas ações) e ___Motivação___ (o que o personagem deseja fazer). Os dois últimos são Livres e podem ser usados a vontade

Após isso, você vai definir o nível de ___Competência___ do personagem em cada um desses Aspectos (e em suas ocupações). O ___Conceito___ sempre terá uma _Competência_ __*Boa (+3)*__. Defina um dos demais como ___Razoável (+2)___ e outros dois como __*Regular (+1)*__.

> Caso o personagem não possua idéia para os dois Aspectos Livres, pode-se deixar os mesmos em branco e serem preenchidos durante o jogo. ___Entretanto___ o personagem deve ter ao menos uma Competência ___Razoável (+2)___ e uma ___Regular (+1)___ associada aos Aspectos

#### Usos dos Aspectos

+ ___Invocação:___ Aumenta as chances dos personagens serem bem-sucedidos, devido aos pontos positivos do mesmo;
    + Exige ___1 Ponto de Destino___, exceto quando o Aspecto tiver ___Usos Gratuítos___ (veja abaixo);
    + +2 em um rolamento, __OU__
    + +2 na dificuldade da oposição fazer algo, __OU__
    + um novo rolamento, __OU__
    + declarar automaticamente alguma situação vantajosa (a critério do narrador)
    + Um Aspecto ___não pode___ ser usado mais de uma vez em uma mesma ação, mas em uma mesma ação pode-se usar vários Aspectos, desde que o personagem tenha Pontos de Destino o bastante;
+ ___Forçada:___ Complica a vida do personagem, mudando os eventos da cena em questão devido a pontos negativos do mesmo
    + Caso seja aceito, ao fim da cena o personagem recebe um Ponto de Destino;
    + Pode ser negado ao custo de 1 Ponto de Destino;

#### Usos Gratuítos e Impulsos

Algumas ações dos Personagens podem fazer com que Aspectos apareçam em jogo, e esses Aspectos possuem _Usos Gratuítos_. Ao fazer uso de tais Aspectos, o personagem ___não precisa___ usar Pontos de Destino, fazendo uso desses Usos. O Aspecto não desaparece se acontecer de tais Usos serem exauridos.

Algumas ações também podem gerar ___Impulsos___, que são Aspectos de curta duração e uso único que representam pequenas vantagens que os personagens obtiveram graças a suas ações. Não há necessidade de pagar Pontos de Destino para usar um Impulso, mas uma vez que o faça o mesmo desaparece

#### Duração dos Aspectos

Em geral, Aspectos permanecem em jogo até que alguma outra ação tente o remover de jogo (normalmente _Superar_: veja abaixo) ou que a mudança de cena leve a tornar os mesmos irrelevantes (um ___Prédio em Chama___ com certeza terá virado cinzas quando os personagens voltarem para lá algumas cenas depois). As exceções são os Aspectos do Personagem (que são alterados em ___Marcos___: veja abaixo), e os Impulsos, que desaparecem tão logo sejam usados.

## Façanhas

Façanhas são as coisas especiais que seu personagem pode fazer, seja por ter treinamento específico, equipamento de melhor qualidade, habilidades especiais, ou qualquer outra justificativa plausível. Todo personagem pode ter até 2 Façanhas antes de começar a pagar Recarga, e as Façanhas seguem o modelo abaixo:

+ _Como sou/tenho [Descreve o que torna você especial], eu recebo +2 ao [Superar/Criar Vantagem/Atacar/Defender] usando [Competência] quando [Situação específica]_; __OU__
+ _Como sou/tenho [Descreve o que torna você especial], uma vez por sessão posso [descreva a coisa especial que você pode fazer]_

### Recarga e Pontos de Destino

___Pontos de Destino___ representam o número de vezes que você pode mudar a narrativa por meio dos seus Aspectos, seja beneficiando rolamentos ou trazendo ___Descrições Narrativas___ ao jogo.

___Recarga___ define o __mínimo__ de ___Pontos de Destino___ que todo personagem tem ao iniciar uma sessão de jogo: pode acontecer, graças às Forçadas, de um personagem começar a sessão seguinte com mais Pontos de Destino que a Recarga, sendo que nesse caso ele mantem os Pontos de Destino que possui.

O Padrão de Recarga é 3, e pode aumentar com a Evolução do Personagem, e cair conforme o personagem compra mais Façanhas que as Façanhas Gratuítas (1 por Façanha Adicional). Exceto se autorizado pelo Narrador, nenhum personagem pode ter Recarga abaixo de 1.

## Rolamentos

Em _Dry Fate_ usa-se 2 pares de dados de seis faces,que chamaremos de dP (dados Positivos) e dN (dados Negativos) e vão indicar o sinal do resultado. 

Role ambos os pares e pegue os dados de menor valor de ambos. Se forem iguais, o valor do rolamento será +0. Caso contrário, considere o resultado como sendo igual ao menor valor, com o sinal equivalendo o tipo de dado.

> ***Exemplos:*** 
> + dP: 4,4; dN: 4,1; menor dP: 4; menor dN: 1; resultado -1
> + dP: 3,2; dN: 3,4; menor dP: 2; menor dN: 3; resultado +2
> + dP: 2,5; dN: 2,4; menor dP: 2; menor dN: 2; resultado +0

> Caso você possua dados Fate, você pode os utilizar normalmente. Além disso, você pode usar a opção do _Baby's First Fudge Dice_ (marcar dados d6 de pontos com caneta permanente com os símbolos de `+`{: .fate_font}, `-`{: .fate_font} e `0`{: .fate_font}) ou usar a emulação padrão (1-2 = `-`{: .fate_font}, 3-4 = `0`{: .fate_font} e 5-6 = `+`{: .fate_font}). Em ambos os caso, cada + adiciona 1 ao resultado e cada - reduz um no resultado de um rolamento, brancos não afetando o mesmo. Qualquer uma dessas formas de rolamento é igualmente funcional
>
> ***NOTA:*** o rolamento em _Dry Fate_ pode retornar valores de +/- 5 no rolamento. Embora fora da escala padrão do Fate, a possibilidade de tal rolamento acontecer é tão pequena que pode ser descartada ou aceita sem nenhuma mudança especial no jogo, ficando a critério do grupo.

Soma-se ao rolamento do dado o valor de ___Competência___ de qualquer ___Ocupação___ que seja aplicável à situação, mais qualquer Façanha aplicável, mais quaisquer usos de Aspectos, e compara-se a uma dificuldade estabelecida, de __*Terrível (-2)*__ até __*Lendário (+8)*__ (sugere-se como dificuldade padrão __*Razoável (+2)*__) para obter-se uma das ___Resoluções___ abaixo:

+ ___Resultado Menor que a dificuldade:___ Falha - Não conseguiu o que deseja-se, ou consegue-se com alguma complicação séria (uma decisão difícil, inimigos percebem suas intenções, etc...);
+ ___Resultado Igual ao da dificuldade:___ Empate - Consegue-se o que deseja, mas com algum incômodo (perde-se ferramentas, os inimigos notam coisas estranhas...)
+ ___Resultado Superior à dificuldade:___ Sucesso - Consegue-se o que deseja sem consequências
+ ___Resultado Superior à dificuldade + 2:___ Sucesso com Estilo - Consegue-se o que deseja e ainda obtêm-se algum benefício adicional (inimigo passa para seu lado, obtem alguma informação adicional...)
    + Por meio de um _Impulso_

> ___Trabalho em Equipe:___ se um personagem possuir uma Competência igual ou similar a do personagem que está realizando algum rolamento, ele pode abdicar de sua ação naquele momento para oferecer um bônus passivo de +1 ao personagem que realiza a ação. O número de personagens que podem fazer isso fica a critério do Narrador.


### Ações Simples

Tudo pode ser descrito em _Dry Fate_ envolvendo um dos quatro tipos de ação abaixo:

+ ___Superar:___ Realizar a ação prevista pela ___Competência___ (Um ___Fotógrafo___ tirar fotos, um ___Médico___ diagnosticar doenças, ...)
    + Essa ação também costuma ser usada para _Remover_ Aspectos, mas não os alterar;
+ ___Criar Vantagens:___ Preparar ou obter algo que poderá ser usado para obter uma vantagem futura como um ___Aspecto___ (Um ___Fotógrafo___ conseguir e equipar ___Lentes Teleobjetivas___, um ___Médico___ solicitar exames adicionais...)
    + Sempre gera ___Aspectos___ com ___Usos Gratuitos___
    + ___Falha___ oferece o uso gratuíto à Oposição
    + ___Sucesso com Estilo___ coloca Duas Invocações Gratuítas
    + Pode ser usado também para tentar ___Mudar___ um Aspecto de jogo
        + ___Guerreiros Hostis___ passam a ser ___Aliados Valorosos___
+ ___Ataque:___ qualquer ação que visa causar um ___Prejuízo___ a outrem (um ___Policial___ golpeia um Ladrão, um ___Bully___ humilha uma criança, um ___CEO___ realiza uma aquisição hostil na bolsa de valores)
    + Normalmente é uma ação que leva a ou é usada em Conflitos (veja abaixo)
+ ___Defesa:___ qualquer tentativa ___ATIVA___ de impedir que qualquer das ações acima sejam realizadas (a ___Criança___ chama a atenção contra o Bully, o ___CEO___ da empresa que sofre a tentativa de aquisição hostil ordena a realização de lucros, tirando as ações da bolsa)

Avalie os reusltados finais conforme os guias mostrados nas Resoluções anteriormente citados.

## Ações Complexas

Quando você precisa resolver situações complexas, você pode utilizar _Desafios_, _Disputas_ e _Conflitos_

### Desafios

Uma série de ações de _Superar_ (encadeadas ou não) que vão resolver um conjunto de ações para alcançar-se um objetivo:

> Desemperrar uma porta para fugir da horda de zumbis ao mesmo tempo em que se monta uma barreira que vá conter os zumbis por tempo o bastante
 
### Disputas

Uma sequência de rolamentos entre dois ou mais lados, visando um objetivo onde apenas um lado pode ser bem sucedido mas os lados não desejam provocar prejuízo direto entre si.

> Vários corredores de rally desejam vencer uma prova importante com uma premiação muito boa.

1. Defina os lados envolvidos e Aspectos do local
2. Defina se existe uma dificuldade mínima (por exemplo: juízes ou o terreno) ou se os personagens estão disputando entre si sem nenhum outro elemento envolvido.
3. Cada lado realiza um rolamento em uma Competência adequado e compara-se seus resultados
    1. O lado com o melhor resultado obtem a ___vitória___ aquela ___altercação___: deve ao menos empatar com qualquer dificuldade mínima.
    2. Se o lado vencedor passou por três ou mais a dificuldade mínima ___ou___ o lado perdedor, ele recebe ___duas___ vitórias
    3. Empates provocam ___Reviravoltas___: condições de vitória o o ambiente mudam, novos inimigos aparecem, etc...
    4. O lado que obtiver três vitórias primeiro vence a Disputa

### Conflito

Quando ambos os lados querem provocar dano ou prejuízo direto de algum tipo ao outro, acontece um conflito

+ Determine os Aspectos as _Zonas_ do local do conflito:
    + Regiões onde as pessoas podem se confrontar
    + Personagens podem se deslocar livremente para qualquer Zona adjacente, mas para zonas mais distantes pode-se demandar rolamentos de Competências adequadas
+ Determina-se quem age primeiro, baseado nas Competências
+ Cada personagem tem direito a realizar uma ação seja de Ataque ou não (pode-se Criar Vantagens, por exemplo)
    + Caso tenha-se efetuado um Ataque, o alvo pode realizar um Rolamento de Defesa
    + Caso a Defesa tenha sido menor que o Ataque, o alvo marca 1 ___Condição___
    + Caso a Defesa tenha tido um ___Sucesso com Estilo___ no Ataque, pode forçar o alvo a Marcar Duas Condições __ou___ marcar uma Condição e obter um Impulso
    + Se o alvo não tiver ou não quiser marcar uma Condição que possa ser marcada para aquele tipo de Conflito, ele foi ___Derrotado___ e tem o destino nas mãos do Atacante
    + Antes dos dados serem rolados, a oposição pode decidir ___Conceder___, saindo do Conflito mas sem todos os prejuízos possíveis:
        + Recebe 1 Ponto de Destino caso Conceda + 1 caso tenha marcado Condições
+ Quem agiu pode escolher qualquer personagem que não tenha agido ainda para ser o próximo a agir (incluindo os personagens do Narrador). Se todos os personagens em cena tiverem agido, o último a agir decide quem será o primeiro no próximo turno
+ O Conflito encerra-se quando um dos lados é totalmente derrotado, concede, ou caso os dois lados decidam de comum acordo;

#### Condições

Cada personagem tem três ___Condições___. Condições são Aspectos que são ___Marcados___ em caso do personagem ser atingido e permanecem em jogo como ___Aspectos___ até que haja tempo e circunstâncias para realizar qualquer ação para limpar essas ___Condições___ (enfaixar uma perna, retomar o fôlego, racionalizar as visões terríveis). O atacante que aplicou uma Condição no personagem recebe um _Uso Gratuíto_ da Condição marcada.

> ***Como nomear as Condições:***
> 
> Existem duas opções interessantes para se nomear suas Condições:
> 
> + ___Pré-Nomear:___ faça com que os personagens tenham suas condições previamente nomeadas, coisas como ___Enfraquecido___, ___Perturbado___, ___Iä! Iä! Cthulhu Fthaghn!___ ou qualquer outra coisa condizente com o que você vai fazer. Ao receber a Condição ele a _Marca_, indicando que ela está ativa, _Limpando-a_ quando recuperar.
>     + ___Vantagens:___ Fica claro o tipo de dano que o personagem tomou, pode ser útil para planejar o tipo de situações, pode permitir ao Narrador formas de restringir abusos
>     + ___Desvantagens:___ perde-se flexibilidade, fica complicado manter, pode colocar facilmente o jogo em situações onde todo o grupo é derrotado em um momento inconveniente
>     
> + ___Pós-Nomear:___ o Aspecto que descreve a Condição é descrito no momento em que a Condição deve ser marcada, e apagado tão logo a Condição seja recuperada. Essa seria a forma padrão do Fate
>     + ___Vantagens:___ Torna os personagens mais resistentes (já que as Condições podem ser usadas conforme a necessidade), permite maior flexibilidade
>     + ___Desvantagens:___ pode permitir que as pessoas "confundam as coisas" conforme o estilo de jogo


> + ___Regra Opcional:___ _Golpes Fulminantes_ - caso o alvo receba um sucesso com estilo ou não tenha mais condições adequadas, ele pode tomar um golpe tão poderoso que pode reescrever um dos Aspectos do personagem. Não marque Condições, mas renomeie um Aspecto de maneira que indique sua nova situação (por exemplo: de ___Policial Indomável___ para ___Policial Indomável Caolho___). Isso não afeta de imediato a Competência relacionado ao mesmo, mas o atacante ainda recebe o _Uso Gratuíto_ nesse novo Aspecto

## NPCs

Em _Dry Fate_ existem apenas dois tipos de NPCs: _Extras_ e _NPCs principais_.

NPCs principais são criados como personagens comuns, apenas mudando sua escala de poder:

+ ___Tenentes___ possuem apenas 1 Condição, 2 Façanhas e 2 Aspectos:
    + ___Conceito___ em _Bom (+3)_
    + ___Motivação___ em _Regular (+1)_
+ ___Chefões___ possuem 3 Condições, 3 Façanhas e 3 Aspectos:
    + ___Conceito___ em _Ótimo (+4)_
    + ___Dificuldade___ em _Regular (+1)_
    + ___Motivação___ em _Bom (+3)_

### Extras

São os ninjas que só servem para apanhar na mão dos heróis, os figurantes, as pessoas-cenário. Não possuem Condições e possuem apenas dois Aspectos, um ___Aspecto Nominal___ (como ___Ninja do Clã do Pé___) e um ___Ponto Fraco___ (como ___Fraquejam quando estão em menor número___).

Normalmente o Aspecto Nominal será _Regular (+1)_ ou _Razoável (+2)_ e o Ponto Fraco será _Terrível (-2)_. Alguns Extras mais fortes podem ter _Bom (+3)_ ou mais no Aspecto Nominal, sendo que nesse caso o Ponto Fraco torna-se apenas _Ruim (-1)_.

#### Horda de Extras

Se você quiser, ajunte vários Extras em uma _Horda_: adicione +1 ao Aspecto Nominal e coloque uma Condição para cada dois Extras. Cada Condição sofrida reduz a Horda o bastante para reduzir o bônus em +1.

### Marcos

São os momentos onde o personagem melhora suas condições após ser bem-sucedido (ou não) em suas ações. Em _Dry Fate_ são dois marcos:

+ ___Marco Menor:___ representa pequenos aprendizados, e normalmente ocorre ao final de uma sessão. Pode-se fazer uma ___E APENAS UMA___ das coisas abaixo
    + Limpar uma Condição
    + Reescrever uma Façanha
    + Comprar uma nova Façanha (caso ainda tenha Façanhas Gratuítas ou Receber +1 em recarga)
    + Alternar os níveis de Competência entre dois Aspectos ___que não envolva o Conceito___
    + Reescrever 1 Aspecto ___que não seja o Conceito___. Isso pode incluir modificar a _Ocupação_ amarrada ao mesmo;
+ ___Marco Maior:___ representa os momentos onde o personagem evolui de maneira maior. Os perigos vencidos já não são tão sérios, e as oposições começam a ficar cada vez maior. Ele pode recebe um ou mais dos benefícios abaixos, quanto o personagem desejar:
    + Recebe 1 Ponto de Recarga
        + Pode ser usado para comprar imediatamente uma nova Façanha
    + Recebe 2 Pontos para distribuir entre suas competências
        + Não pode-se elevar o nível do Conceito, ao menos que pelo menos 1 outra Competência tenha sido elevada acima do Conceito;
    + Pode-se renomear o Conceito, incluindo sua _Ocupação_;
    + Recebe-se um benefício de um Marco Menor

## Exemplo de Personagem

### Thyrar, o Halfling ladino

#### Aspectos

| ___Tipo:___ | ___Aspecto___ | ___Competência___ |
|-:|:-|:-:|
| ___Conceito___ | Um ___Ladino halfling___ esquivo | ___Bom (+3)___ |
| ___Dificuldade___ | Os ___Comunais___ dos Montes Fair Mellows já estavam começando a me importunar | ___Regular (+1)___ |
| ___Motivação___ | Um Halfling que quer conhecer o mundo | ___Razoável (+2)___ |
|  | Alyeda, Deusa da Sorte e do Destino, ilumina seus ___Seguidores___ | ___Razoável (+2)___ |
| | Fezair precisa entender que ___Ilusões não me afetam___ | ___Regular (+1)___ |

#### Façanhas [Recarga: 3]

+ Como ladino, sei usar ___Ataques Furtivos___, então ___uma vez por sessão___ posso golpear um alvo que não tenha me notado, como se ele tivesse apenas ___Medíocre (+0)___ ao se defender
+ Tenho ___Contatos no submundo___, então recebo +2 ao ___Criar Vantagens___ enquanto tento obter informações no meio do submundo;

